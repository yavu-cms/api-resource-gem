# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'yavu/api/resource/version'

Gem::Specification.new do |spec|
  spec.name          = 'yavu-api-resource'
  spec.version       = Yavu::API::Resource::VERSION
  spec.authors       = ['Christian Rodriguez']
  spec.email         = ['car@cespi.unlp.edu.ar']
  spec.description   = IO.read('README.md')
  spec.summary       = %q{Provides serializable entities to be used between Backend & Frontend communication.}

  spec.homepage      = ''
  spec.license       = 'MIT'

  spec.metadata['allowed_push_host'] = "http://gems.desarrollo.unlp.edu.ar" if spec.respond_to?(:metadata)

  spec.files         = `git ls-files`.split($/)
  spec.executables   = spec.files.grep(%r{^bin/}) { |f| File.basename(f) }
  spec.test_files    = spec.files.grep(%r{^(test|spec|features)/})
  spec.require_paths = ['lib']

  spec.add_development_dependency 'bundler',            '~> 1.10'
  spec.add_development_dependency 'rake'
  spec.add_development_dependency 'cespigems',          '~> 0.1'
  spec.add_development_dependency 'minitest',           '~> 4.7.5'
  spec.add_development_dependency 'minitest-reporters', '~> 0.14.24'
  spec.add_development_dependency 'mocha',              '~> 1.0.0'
  spec.add_development_dependency 'simplecov',          '~> 0.8.2'
  spec.add_development_dependency 'simplecov-console',  '~> 0.1.3'
  spec.add_development_dependency 'simplecov-rcov',     '~> 0.2.3'

  spec.add_runtime_dependency 'rack',          '~> 1.5.2'
  spec.add_runtime_dependency 'activesupport', '~> 4.0.0'
  spec.add_runtime_dependency 'multi_json',    '~> 1.7.9'
  spec.add_runtime_dependency 'oj',            '~> 2.1.7'
end

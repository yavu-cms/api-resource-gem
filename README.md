# Yavu::Api::Resource

Provides serializable entities to be used between 
Backend & Frontend communication.

## Installation

Add this line to your application's Gemfile:

    gem 'yavu-api-resource'

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install yavu-api-resource

## Usage

YAVU Backend and frontend will use this gem to render real ActiveRecord models 
that will be converted to Yavu::API::Resource::Base entities

This entities will be dumped as JSON and unserialized by Frontend from JSON
preserving its properties and behaviour in both Frontend & Backend

### Backend integration

Only backend manipulates Active Record models, so this gem provides 
**Yavu::API::Resource::Resourceable** module that must be included by ActiveRecord
in order to provide `set_api_resource` class method in your AR models

Only requiring the `require 'yavu/api/ext/active_record/base` file you 
will have access to `set_api_resource` in your models 

or 

simply adding the following lines in your code

```
require 'yavu/api/resource/resourceable'
module ActiveRecord
  class Base
    include API::Resource::Resourceable
  end
end
```

#### Using `set_api_resource`

You can add this class method to your classes in order to specify which fields
your API::Resource models will consider. If you don't, they will only have the
id field

```
class Article < ActiveRecord::Base
  # Attributes to be exported for API
  set_api_resource id: :slug,
    slug: :slug,
    title: :title,
    lead: :lead,
    body: :body,
    main_image: :main_image,
    image_media: ->(x) { article_media.image.limit(api_resource_settings[:media_limit]) if api_resource_settings[:media_limit]},
    embedded_media: ->(x) { article_media.embedded.limit(api_resource_settings[:media_limit]) if api_resource_settings[:media_limit] },
    file_media: ->(x) { article_media.file.limit(api_resource_settings[:media_limit]) if api_resource_settings[:media_limit] },
    audio_media: ->(x) { article_media.audio.limit(api_resource_settings[:media_limit]) if api_resource_settings[:media_limit] },
    tags: ->(x){ tags.limit(api_resource_settings[:tags_limit]) if api_resource_settings[:tags_limit] },
    related: ->(x) {related.limit(api_resource_settings[:related_limit]) if api_resource_settings[:related_limit] }
```
As you can see in the above sample, we are exposing only:

* `id`: with the slug
* `title`: with article's title
* `lead`: with article's lead
* `body`: with article's body
* `main_image`: with article's main_image (other resorce)
* `image_media`: is a proc that will return all article_media images limited to
  a presetted **api_resource_settings media_limit**. If this setting is not
setted, **NO MEDIAS will be returned**
* ...


## Contributing

1. Fork it
2. Create your feature branch (`git checkout -b my-new-feature`)
3. Commit your changes (`git commit -am 'Add some feature'`)
4. Push to the branch (`git push origin my-new-feature`)
5. Create new Pull Request

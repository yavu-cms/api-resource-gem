# Custom options and filters for SimpleCov
require 'simplecov-console'
require 'simplecov-rcov'
require 'colorize'
require 'active_support/core_ext'

# Aim high :)
MINIMUM_COVERAGE_PERCENT = 50

SimpleCov.command_name 'test'
SimpleCov.minimum_coverage MINIMUM_COVERAGE_PERCENT

SimpleCov.add_filter '/test/'
SimpleCov.add_group 'Resource' do |src|
  File.dirname(src.filename) =~ /lib\/yavu\/api(\/resource)?\z/
end
SimpleCov.add_group 'Context',    'lib/yavu/api/resource/context'
SimpleCov.add_group 'Models',     'lib/yavu/api/resource/model'
SimpleCov.add_group 'Extensions', 'lib/yavu/api/ext'

# Provide output for both the Console and the Browser
formatters = [SimpleCov::Formatter::HTMLFormatter]
formatters << SimpleCov::Formatter::RcovFormatter if ENV['CI']
formatters << SimpleCov::Formatter::Console unless ENV['CI']
SimpleCov.formatter = SimpleCov::Formatter::MultiFormatter[*formatters]

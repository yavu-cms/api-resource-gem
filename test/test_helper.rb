require 'simplecov'
SimpleCov.start

require 'yavu/api/resource'
require 'minitest/spec'
require 'minitest/pride'
require 'minitest/autorun'
require 'mocha/mini_test'
require 'minitest/reporters'

MiniTest::Reporters.use! ENV['CI'] ? Minitest::Reporters::SpecReporter.new : Minitest::Reporters::ProgressReporter.new
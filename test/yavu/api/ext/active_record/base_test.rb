require 'test_helper'

describe 'ActiveRecord::Base' do
  before do
    module ActiveRecord
      class Base
        def self.include(*modules)
          @@included_modules = modules
        end

        def self.included_modules
          @@included_modules
        end
      end
    end

    require 'yavu/api/ext/active_record/base'
  end

  describe 'module inclusion' do
    it 'extends ActiveRecord::Base with Yavu::API::Resource::Resourceable' do
      ActiveRecord::Base.included_modules.must_include Yavu::API::Resource::Resourceable
    end
  end
end
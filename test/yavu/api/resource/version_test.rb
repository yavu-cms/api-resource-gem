require 'test_helper'
require 'minitest/spec'
require 'yavu/api/resource/version'

describe Yavu::API::Resource::VERSION do
  it 'exists' do
    Yavu::API::Resource::VERSION.wont_be_nil
  end
end
require 'test_helper'
require 'minitest/spec'

describe Yavu::API::Resource::Medium do
  describe '#type' do
    let(:medium) { Yavu::API::Resource::Medium.new }

    it 'returns an empty symbol' do
      medium.type.must_equal :''
    end
  end
end
require 'test_helper'
require 'minitest/spec'

describe Yavu::API::Resource::Article do
  describe '#main_image?' do
    let(:article) { Yavu::API::Resource::Article.new }

    describe 'when the article has a main image' do
      before { article.expects(:main_image).returns(stub).once }

      it 'returns whether the main image is present' do
        article.main_image?.must_equal true
      end
    end

    describe "when the article doesn't have a main image" do
      before { article.expects(:main_image).returns(nil).once }

      it 'returns false' do
        article.main_image?.must_equal false
      end
    end
  end
end
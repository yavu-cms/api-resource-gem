require 'test_helper'
require 'minitest/spec'

describe Yavu::API::Resource::ImageMedium do
  describe '#type' do
    let(:medium) { Yavu::API::Resource::ImageMedium.new }

    it 'returns :image' do
      medium.type.must_equal :image
    end
  end
end
require 'test_helper'
require 'minitest/spec'

describe Yavu::API::Resource::EmbeddedMedium do
  describe '#type' do
    let(:medium) { Yavu::API::Resource::EmbeddedMedium.new }

    it 'returns :embedded' do
      medium.type.must_equal :embedded
    end
  end
end
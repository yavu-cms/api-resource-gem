require 'test_helper'
require 'minitest/spec'

describe Yavu::API::Resource::AudioMedium do
  describe '#type' do
    let(:medium) { Yavu::API::Resource::AudioMedium.new }

    it 'returns :audio' do
      medium.type.must_equal :audio
    end
  end
end
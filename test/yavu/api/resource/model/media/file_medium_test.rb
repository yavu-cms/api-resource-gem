require 'test_helper'
require 'minitest/spec'

describe Yavu::API::Resource::FileMedium do
  describe '#type' do
    let(:medium) { Yavu::API::Resource::FileMedium.new }

    it 'returns :file' do
      medium.type.must_equal :file
    end
  end
end
require 'test_helper'
require 'minitest/spec'

describe Yavu::API::Resource::ProcessedContext do
  describe '#initialize' do
    describe 'with no args' do
      let(:processed_context) { Yavu::API::Resource::ProcessedContext.new }

      it 'has no errors, components nor headers' do
        processed_context.errors.must_be_empty
        processed_context.components.must_be_empty
        processed_context.headers.must_be_instance_of Rack::Utils::HeaderHash
        processed_context.headers.must_be_empty
      end
    end

    describe 'with args' do
      let(:processed_context) { Yavu::API::Resource::ProcessedContext.new args }
      let(:args) { Hash[errors: errors, components: components, headers: headers] }
      let(:errors) { nil }
      let(:components) { nil }
      let(:headers) { nil }

      describe 'with errors' do
        let(:errors) { ['Some error just happened', 'Watch out!'] }

        it 'keeps the errors' do
          processed_context.errors.must_equal errors
        end
      end

      describe 'with components' do
        describe 'when component keys are valid integer representations' do
          let(:components) { Hash['1' => 'Component 1', :'21' => 'Component 2', 35 => 'Component 3'] }
          let(:expected) { Hash[1 => 'Component 1', 21 => 'Component 2', 35 => 'Component 3'] }

          it 'normalizes the components' do
            processed_context.components.must_equal expected
          end
        end

        describe 'when component keys are not valid integer representations' do
          let(:components) { { 'a' => 'Component A' } }

          it 'raises an ArgumentError' do
            -> { processed_context }.must_raise ArgumentError
          end
        end
      end

      describe 'with headers' do
        let(:headers) { Hash['Content-Type' => 'text/html', 'X-Some-Custom-Header' => 'Yavu'] }

        it 'creates a HeaderHash with the headers' do
          processed_headers = processed_context.headers
          processed_headers.must_be_instance_of Rack::Utils::HeaderHash
          processed_headers['Content-Type'].must_equal 'text/html'
          processed_headers['X-Some-Custom-Header'].must_equal 'Yavu'
        end
      end

      describe 'with errors, components and headers' do
        let(:errors) { ['Darn, another error!'] }
        let(:components) { Hash['23' => 'A component'] }
        let(:headers) { Hash['Content-Length' => 10] }

        it 'keeps and normalizes all of them' do
          processed_context.errors.must_equal errors
          processed_context.components.must_equal 23 => 'A component'
          processed_context.headers.must_equal 'Content-Length' => 10
        end
      end
    end
  end

  describe '#after_from_hash' do
    let(:processed_context) { Yavu::API::Resource::ProcessedContext.new }

    describe 'when neither components nor headers are set' do
      it 'sets them to correct empty values' do
        processed_context.after_from_hash
        processed_context.components.must_be_empty
        processed_context.headers.must_be_instance_of Rack::Utils::HeaderHash
        processed_context.headers.must_be_empty
      end
    end

    describe 'when components and headers are set' do
      before do
        processed_context.components = {'4' => 'Dat component'}
        processed_context.headers = {'Content-Type' => 'text/plain'}
      end

      it 'normalizes the components and headers' do
        processed_context.after_from_hash
        processed_context.components.must_equal 4 => 'Dat component'
        processed_context.headers.must_be_instance_of Rack::Utils::HeaderHash
        processed_context.headers.must_equal 'Content-Type' => 'text/plain'
      end
    end
  end

  describe '#add_processed_component' do
    let(:new_context) { Hash[some: 'value'] }

    describe 'when the component exists' do
      let(:processed_context) { Yavu::API::Resource::ProcessedContext.new }

      it 'adds an evaluated context for the given component index (id)' do
        processed_context.add_processed_component 5, new_context
        processed_context.components[5].must_equal new_context
      end
    end

    describe "when the component doesn't exist" do
      let(:processed_context) { Yavu::API::Resource::ProcessedContext.new components: {5 => old_context} }
      let(:old_context) { Hash.new }

      it 'overwrites its evaluated context' do
        processed_context.add_processed_component 5, new_context
        processed_context.components[5].must_equal new_context
      end
    end
  end

  describe '#processed_component' do
    describe 'when the component exists' do
      let(:processed_context) { Yavu::API::Resource::ProcessedContext.new components: {5 => context} }
      let(:context) { Hash[some: 'value'] }

      it 'returns the context for that component index (id)' do
        processed_context.processed_component(5).must_equal context
      end
    end

    describe "when the component doesn't exist" do
      let(:processed_context) { Yavu::API::Resource::ProcessedContext.new }

      it 'returns nil' do
        processed_context.processed_component(5).must_be_nil
      end
    end
  end

  describe '#_locals' do
    let(:processed_context) { Yavu::API::Resource::ProcessedContext.new }
    let(:values) { Hash[errors: [], headers: {}] }
    let(:expected) { Hash[errors: [], headers: {}, _context: processed_context] }

    before do
      processed_context.expects(:_values).returns(values).once
    end

    it 'adds a :_context key to the values of the context' do
      processed_context._locals.must_equal expected
    end
  end

  describe '#_values' do
    let(:processed_context) { Yavu::API::Resource::ProcessedContext.new components: {9 => 'A component'} }
    let(:expected) { Hash[errors: [], headers: {}] }

    it 'returns all values except :components' do
      processed_context._values.must_equal expected
    end
  end

  describe '#permitted_scalar?' do
    let(:processed_context) { Yavu::API::Resource::ProcessedContext.new }

    describe 'when the value is one of the scalar permitted by the parent class' do
      it 'returns true' do
        Yavu::API::Resource::Base::PERMITTED_SCALAR_TYPES.each do |type|
          value = case
                    when type == Symbol
                      :some_symbol
                    when type == NilClass
                      nil
                    when type == TrueClass
                      true
                    when type == FalseClass
                      false
                    else
                      type.new
                  end
          processed_context.permitted_scalar?(value).must_equal true
        end
      end
    end

    describe 'when the value is of an unsupported type' do
      before do
        class IJustMadeThisUp; end
      end

      it 'returns false' do
        processed_context.permitted_scalar?(IJustMadeThisUp.new).must_equal false
      end
    end

    describe 'when ActionDispatch::Http::Headers is present' do
      before do
        module ActionDispatch
          module Http
            class Headers
            end
          end
        end
      end

      it 'permits ActionDispatch::Http::Headers objects' do
        processed_context.permitted_scalar?(ActionDispatch::Http::Headers.new).must_equal true
      end
    end

    describe 'when ActionDispatch::Http::Headers is not present' do
      before do
        class IJustMadeThisUpToo; end
      end

      it "does nothing (but doesn't fail either)" do
        refute processed_context.permitted_scalar?(IJustMadeThisUpToo.new)
      end
    end
  end
end
require 'active_support/core_ext'
require 'rack/utils'

require 'yavu/api/resource/version'
require 'yavu/api/resource/base'
require 'yavu/api/resource/user'
require 'yavu/api/resource/model/medium'
require 'yavu/api/resource/model/route'
require 'yavu/api/resource/context/processed_context'

%w( yavu/api/resource/context/*.rb yavu/api/resource/model/**/*rb ).each do |path|
  Gem.find_files(path).each {|p| require p}
end

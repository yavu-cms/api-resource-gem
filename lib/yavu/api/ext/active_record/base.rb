require 'yavu/api/resource/resourceable'

class ActiveRecord::Base
  include Yavu::API::Resource::Resourceable
end
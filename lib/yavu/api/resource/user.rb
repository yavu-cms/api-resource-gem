module Yavu::API::Resource
  class User < Base
    def cache_key
      "#{self.class.to_s}/#{uid}-#{username}-#{login}"
    end
  end
end

module Yavu::API::Resource
  class Medium < Base
    def type
      self.class.to_s.demodulize.chomp('Medium').underscore.to_sym
    end
  end
end

module Yavu::API::Resource
  class Article < Base
    def main_image?
      !main_image.nil?
    end
  end
end
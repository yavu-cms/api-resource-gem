require 'uri'

module Yavu::API::Resource
  class SearchQuery < Base
    def build_path(base)
      uri = URI(base)
      params = [uri.query]
      %w( query date section_name page order_by ).each do |param|
        params << "#{param}=#{URI.escape send(param).to_s}" if send(param).present?
      end
      uri.query = params.reject(&:blank?).join('&')
      uri.to_s
    end
  end
end

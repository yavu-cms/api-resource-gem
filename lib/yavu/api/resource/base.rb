require 'ostruct'
require 'multi_json'
if RUBY_PLATFORM != "java"
require 'oj'
else
require 'json'
end

module Yavu
  module API
    module Resource
      class Base < OpenStruct
        PERMITTED_SCALAR_TYPES = [
          String,
          Symbol,
          NilClass,
          Numeric,
          TrueClass,
          FalseClass,
          Date,
          Time,
          StringIO,
          Yavu::API::Resource::Base
        ]

        PERMITTED_COLLECTION = [
          Array,
          Hash
        ]

        def initialize(hash={})
          super
          self.errors = Array(hash[:errors])
        end

        def prepare_json
          {
            'type'   => self.class.name.demodulize,
            'fields' => Hash[@table.map { |k, v| [k, prepare_json_value(v)] }]
          }
        end

        def to_json
          prepare_json.to_json
        end

        def self.from_hash(o, shared_refs = {})
          case
            when o =~ /^@ref:[A-Z].*:-?\d+.*$/
              h = shared_refs[o]
              if h.is_a?(Hash)
                h = from_hash h, shared_refs
                shared_refs[o] = h
              end
              h
            when o.is_a?(Hash)
              unless o['type'].blank?
                "Yavu::API::Resource::#{o['type']}".constantize.new.tap do |object|
                  o['fields'].each do |k, v|
                    object[k] = from_hash(v, shared_refs)
                  end
                  object.after_from_hash
                end
              else
                Hash[o.map { |k, v| [k, from_hash(v, shared_refs)] }].with_indifferent_access
              end
            when o.is_a?(Array)
              o.map { |x| from_hash x, shared_refs }
            else
              o
          end
        end

        def self.from_json(o)
          h = MultiJson.load(o)
          shared_refs = extract_shared_refs h
          from_hash h, shared_refs
        end

        def []=(name, value)
          permitted_type? value
          super
        end

        def method_missing(mid, *args)
          mname = mid.id2name
          len = args.length
          if mname.chomp!('=') && len >=1
            permitted_type? args[0]
          end
          super
        end

        def errors?
          self.errors.any?
        end

        protected

        def self.extract_shared_refs(h)
          h['fields'].try :delete, 'shared_refs'
        rescue
          {}
        end

        def after_from_hash;
        end

        def prepare_json_value(v)
          case
            when permitted_scalar?(v)
              v.respond_to?(:prepare_json) ? v.prepare_json(*args) : v
            when v.is_a?(Hash)
              Hash[v.map { |k, nv| [k, prepare_json_value(nv)] }]
            else
              v.map { |x| prepare_json_value(x) }
          end
        end


        def permitted_scalar?(value)
          PERMITTED_SCALAR_TYPES.any? { |type| value.is_a?(type) }
        end

        def permitted_collection?(value)
          permitted = PERMITTED_COLLECTION.any? { |type| value.is_a?(type) }
          value = value.values if value.is_a?(Hash)
          permitted && value.all? { |x| permitted_type? x }
        end

        def permitted_type?(value)
          return true if permitted_scalar?(value) || permitted_collection?(value)
          raise ArgumentError, "#{self.class.name} not support #{value.class.name} as member"
        end

        def new_ostruct_member(name)
          name = name.to_sym
          unless respond_to?(name)
            define_singleton_method(name) { @table[name] }
            define_singleton_method("#{name}=") { |x| permitted_type?(x); modifiable[name] = x }
          end
          name
        end
      end
    end
  end
end

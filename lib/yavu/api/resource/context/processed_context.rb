module Yavu::API::Resource
  class ProcessedContext < Base
    def initialize(args={})
      super
      self.components = Hash[(args[:components] || {}).map { |k, v| [Integer(k.to_s), v] }]
      self.headers = Rack::Utils::HeaderHash.new (args[:headers] || {})
    end

    def after_from_hash
      self.components = Hash[self.components.map { |k, v| [k.to_i, v] }]
      self.headers = Rack::Utils::HeaderHash.new (self.headers || {})
      self.last_modified = self.last_modified.try(:to_date)
    end

    # Add a new processed context related to a component_configuration
    def add_processed_component(index, evaluated_component_context)
      components[index] = evaluated_component_context
    end

    # Returns a processed context for specified component index
    def processed_component(index)
      components[index]
    end

    # Return set variables for components
    def _locals
      _values.merge({_context: self})
    end

    def _values
      @table.reject { |k, v| k == :components }
    end

    def permitted_scalar?(value)
      super || defined?(ActionDispatch::Http::Headers) && value.is_a?(ActionDispatch::Http::Headers) || value.is_a?(Set)
    end
  end
end

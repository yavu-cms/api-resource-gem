module Yavu::API::Resource
  class PreprocessedContext < ProcessedContext
    # Add a new processed context related to a component_configuration
    def add_processed_component(index, evaluated_component_context)
      components[index] = evaluated_component_context
    end

    def permitted_scalar?(value)
      super || value.is_a?(ActiveRecord::Base) || value.is_a?(ActiveRecord::Relation)
    end

    # Keys that must be ignored by shared references
    # caching because it causes a wrong behavior. By now,
    # services are the ignored
    def ignored_key?(k)
      %i(services).include? k
    end

    # Finished pre processing. Returns a processed context
    def stop_processing(processor)
      shared_refs = Hash.new
      _stop_processing(shared_refs).tap do |p|
        p[:shared_refs] = shared_refs
        p.last_modified = [p.last_modified, processor.root.try(:updated_at)].grep(Time).max
      end
    end

    def as_api_resource(v, shared_refs = {}, k = nil)
      case
        when v.respond_to?(:as_api_resource)
          store_or_link(v, :object_id, shared_refs) { v.as_api_resource }
        when v.is_a?(Hash)
          if ignored_key?(k)
            Hash[v.map { |kv, vv| [kv, as_api_resource(vv, shared_refs, k)] }].with_indifferent_access
          else
            store_or_link(v, :hash, shared_refs) do
              Hash[v.map { |kv, vv| [kv, as_api_resource(vv, shared_refs, k)] }].with_indifferent_access
            end
          end
        when v.respond_to?(:map)
          v.map { |x| as_api_resource(x, shared_refs, k) }
        else
          v
      end
    end

    protected

    def store_or_link(value, key_method, shared_refs, &value_resourcing)
      link_key = "@ref:#{value.class.name.demodulize}:#{value.send(key_method)}"
      shared_refs[link_key] ||= yield
      link_key
    end

    def _stop_processing(shared_refs = {})
      ProcessedContext.new.tap do |processed|
        _values.reject {|k,_| k == :client_application || k == :route }.each do |k, v|
          _v_updated_at = v.try(:updated_at) || 1.year.ago
          max_updated_at ||= _v_updated_at
          max_updated_at = _v_updated_at if _v_updated_at > max_updated_at
          begin
            processed[k] = as_api_resource(v, shared_refs, k)
          rescue Exception => e
            raise Exception.new "Error at stop_processing for value [#{k}]: #{e.message}"
          end
        end
        processed.components = Hash[self.components.map do |k, v|
          begin
            [k, v._stop_processing(shared_refs)]
          rescue Exception => e
            raise Exception.new "Error at stop_processing for component [#{k}]: #{e.message}"
          end
        end]
        processed[:last_modified] = (processed.components.values.map(&:last_modified) + [ max_updated_at ]).max
        processed.components.each do |k,v|
          v.delete_field :last_modified
        end
      end
    end
  end
end

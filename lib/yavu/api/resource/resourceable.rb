# Add support for ActiveRecord::Base models to be converted to API::Resource::Models
# When a class includes module: API::Resourceable then it can use:
# as_api_resource field: :new_field, field2: ->(x) {|x| a block }a, ....
module Yavu::API::Resource
  module Resourceable
    @@_cache_enabled = false

    def self.cache_enabled
      @@_cache_enabled
    end

    def self.cache_enable!
      @@_cache_enabled = true
    end

    def self.included(base)
      base.extend(ClassMethods)
      base.set_api_resource_class
      base.set_api_resource id: :id
      base.send :define_method, :api_limit_for, &->(model, limit) do
        ars = "#{model}_limit".to_sym
        if api_resource_settings[ars].to_i < limit
          api_resource_settings[ars] = limit
        end
      end
      base.send :attr_accessor, :api_resource_settings
      base.after_initialize { self.api_resource_settings = {} }
    end

    module ClassMethods
      def set_api_resource_class(klass = nil)
        define_method 'api_resource_class' do
          "Yavu::API::Resource::#{(klass || self.class).name}".safe_constantize || Yavu::API::Resource::Base
        end
      end

      def set_api_resource(options)
        options = options.with_indifferent_access
        define_method "as_api_resource" do
            values = Hash.new
            options.each do |attr,value|
              calculated_value = case
                when value.is_a?(Symbol)
                  self.send value
                when value.is_a?(Proc)
                  self.instance_eval(&value)
                else raise ArgumentError.new 'as_api_resource expects symbols or lambdas'
              end
              # Recursively map as_api_resource related AR values
              values[attr] = case
              when calculated_value.respond_to?(:as_api_resource)
                calculated_value.as_api_resource
              when calculated_value.is_a?(Hash)
                Hash[calculated_value.map { |k,x| [k, (x.respond_to?(:as_api_resource) ?  x.as_api_resource : x)] }].with_indifferent_access
              when calculated_value.respond_to?(:map)
                calculated_value.map { |x| x.respond_to?(:as_api_resource) ?  x.as_api_resource : x }
              else
                calculated_value
              end
            end
            api_resource_class.new(values)
        end

        if Yavu::API::Resource::Resourceable.cache_enabled
          define_method "cache_key_api_resource" do
            api_resource_settings.to_s.parameterize
          end

          define_method "as_api_resource_with_cache" do
            Rails.cache.fetch("#{cache_key}/as_api_resource/#{cache_key_api_resource}") do
              as_api_resource_without_cache
            end
          end

          alias_method_chain :as_api_resource, :cache if method_defined? :as_api_resource
        end
      end
    end
  end
end

